Feature: Verify Adserver Response for VPAID TAG with Geo parameters

  
  JIRA Ticket ID: CSCM-3608

  @init
  Scenario: Get VPAID AdTag for Verification
    Given I fetch a existing VPAID tag

  Scenario Outline: Verify API Response for VPIAD tag for Geo
    Given User gets the "<paramName>"from config data
    When  User post a get request to the updated VPAID tag for Geo
    Then  Returned response shoulde have status code as 200 and content-type as XML for Geo param
    And   Returned response should have Geo params "<paramName>" same as config value
    
     Examples: 
      |paramName   |
      |GEO         |