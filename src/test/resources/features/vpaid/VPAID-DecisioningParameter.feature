Feature: Verify Adserver Response for VPAID TAG with Descioning parameters
  USE_IAB,
  USE_IAS,
  USE_DV,
  USE_DVPPD
  USE_AD_CHOICES,
  DV_PPD_TIMEOUT,
  IAS_TIMEOUT,
  IAS_ADV_ENTITY_ID,
  IAS_PUB_ENTITY_ID,
  
  
  JIRA Ticket ID: CSCM-3608

  @init
  Scenario: Get VPAID AdTag for Verification
    Given I fetch a existing VPAID tag

  Scenario Outline: Verify API Response for VPIAD tag for USE_IAB param
    Given user updates the parameter "<paramName>" to "<modifiedValue>"
    When user post a get request to the updated VPAID tag
    Then returned response shoulde have status code as 200 and content-type as XML
    And returned response should have "<paramName>" param value as "<modifiedValue>"

    Examples: 
      | paramName | modifiedValue |
      | USE_IAB   | true          |
      | USE_IAB   | false         |

  Scenario Outline: Verify API Response for VPIAD tag for USE_IAS param
    Given user updates the parameter "<paramName>" to "<modifiedValue>"
    When user post a get request to the updated VPAID tag
    Then returned response shoulde have status code as 200 and content-type as XML
    And returned response should have "<paramName>" param value as "<modifiedValue>"

    Examples: 
      | paramName | modifiedValue |
      | USE_IAS   | false         |
      | USE_IAS   | true          |

  Scenario Outline: Verify API Response for VPIAD tag for USE_DV param
    Given user updates the parameter "<paramName>" to "<modifiedValue>"
    When user post a get request to the updated VPAID tag
    Then returned response shoulde have status code as 200 and content-type as XML
    And returned response should have "<paramName>" param value as "<modifiedValue>"

    Examples: 
      | paramName | modifiedValue |
      | USE_DV    | false         |
      | USE_DV    | true          |

  Scenario Outline: Verify API Response for VPIAD tag for USE_DV_PPD param
    Given user updates the parameter "<paramName>" to "<modifiedValue>"
    When user post a get request to the updated VPAID tag
    Then returned response shoulde have status code as 200 and content-type as XML
    And returned response should have "<paramName>" param value as "<modifiedValue>"

    Examples: 
      | paramName  | modifiedValue |
      | USE_DV_PPD | false         |
      | USE_DV_PPD | true          |

  Scenario Outline: Verify API Response for VPIAD tag for USE_AD_CHOICES param
    Given user updates the parameter "<paramName>" to "<modifiedValue>"
    When user post a get request to the updated VPAID tag
    Then returned response shoulde have status code as 200 and content-type as XML
    And returned response should have "<paramName>" param value as "<modifiedValue>"

    Examples: 
      | paramName      | modifiedValue |
      | USE_AD_CHOICES | false         |
      | USE_AD_CHOICES | true          |

  Scenario Outline: Verify API Response for VPIAD tag for DV_PPD_TIMEOUT param
    Given user updates the parameter "<paramName>" to "<modifiedValue>"
    And user selects DoubleVerify(Fraud) as true
    When user post a get request to the updated VPAID tag
    Then returned response shoulde have status code as 200 and content-type as XML
    And returned response should have "<paramName>" param value as "<modifiedValue>"

    Examples: 
      | paramName      | modifiedValue |
      | DV_PPD_TIMEOUT | false         |
      | DV_PPD_TIMEOUT | true          |

  Scenario Outline: Verify API Response for VPIAD tag for IAS_TIMEOUT param
    Given user updates the parameter "<paramName>" to "<modifiedValue>"
    And user selects Integral Ad Science(IAS) Firewall as true
    When user post a get request to the updated VPAID tag
    Then returned response shoulde have status code as 200 and content-type as XML
    And returned response should have "<paramName>" param value as "<modifiedValue>"

    Examples: 
      | paramName   | modifiedValue |
      | IAS_TIMEOUT | false         |
      | IAS_TIMEOUT | true          |

  Scenario Outline: Verify API Response for VPIAD tag for IAS_ADV_ENTITY_ID & IAS_PUB_ENTITY_ID  param
    Given user updates the parameter for IAS BrandSafety "<paramName>" and "<paramName1>" to "<modifiedValue>" and "<modifiedValue1>"
    And user selects Integral Ad Science(IAS) Firewall as true
    When user post a get request to the updated VPAID tag
    Then returned response shoulde have status code as 200 and content-type as XML
    And returned response should have "<paramName>" and "<paramName1>"  param value as "<modifiedValue>" and "<modifiedValue1>" for IAS BrandSafety

    Examples: 
      | paramName         | modifiedValue | paramName1      |modifiedValue1      |
      | IAS_ADV_ENTITY_ID |         96700 |IAS_PUB_ENTITY_ID|17062117            |
      | IAS_ADV_ENTITY_ID |         96703 |IAS_PUB_ENTITY_ID|17062172						 |
      | IAS_ADV_ENTITY_ID |         99099 |IAS_PUB_ENTITY_ID|17062173						 |
      | IAS_ADV_ENTITY_ID |         96696 |IAS_PUB_ENTITY_ID|17067313						 |
      | IAS_ADV_ENTITY_ID |         12345 |IAS_PUB_ENTITY_ID|1706731356					 |

