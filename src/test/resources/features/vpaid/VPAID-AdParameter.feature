Feature: Verify Adserver Response for VPAID TAG with ad parameters

  
  JIRA Ticket ID: CSCM-3608

  @init
  Scenario: Get VPAID AdTag for Verification
    Given I fetch a existing VPAID tag

  Scenario Outline: Verify API Response for VPIAD tag for Ad tag URL
    Given User gets the ad tag url "<paramName>"from config data
    When  User post a get request to the updated VPAID tag for Ad
    Then  Returned response shoulde have status code as 200 and content-type as XML for Ad param
    And   Returned response should have ad params "<paramName>" same as config value
    
     Examples: 
      |paramName   |
      |EXTERNAL_URL|