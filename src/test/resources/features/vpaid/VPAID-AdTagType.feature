Feature: Verify Adserver Response for VPAID TAG with AdType parameters

  
  JIRA Ticket ID: CSCM-3608

  @init
  Scenario: Get VPAID AdTag for Verification
    Given I fetch a existing VPAID tag

  Scenario Outline: Verify API Response for VPIAD tag for AdType & GEO param
    Given user gets the ad params "<paramName>"from config data
    When  user post a get request to the updated VPAID tag for Ads parameter
    Then  returned response shoulde have status code as 200 and content-type as XML for AdtagType param
    And   returned response should have ad params "<paramName>" same as config value
    
     Examples: 
      |paramName   |
      |AD_TYPE     |
   
 
     
             
    				
      
      