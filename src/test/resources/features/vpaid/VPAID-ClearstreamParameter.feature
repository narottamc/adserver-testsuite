Feature: Verify Adserver Response for VPAID TAG with clearstream parameters
  AGENCY_ID,
  ASSET_ID,
  BRAND_ID,
  CAMPAIGN_ID,
  FLIGHT_ID,
  PLACEMENT_ID,
  ADV_CPM,
  AD_ID,
  
  
  JIRA Ticket ID: CSCM-3608

  @init
  Scenario: Get VPAID AdTag for Verification
    Given I fetch a existing VPAID tag

  Scenario Outline: Verify API Response for VPIAD tag for AGENCY_ID,BRAND_ID,CAMPAIGN_ID,FLIGHT_ID,PLACEMENT_ID,ADV_CPM,AD_ID param
    Given user gets the clearstream par "<paramName>" from config data
    When  user post a get request to the updated VPAID tag for clearstream parameters
    Then  returned response shoulde have status code as 200 and content-type as XML for clearstream param
    And   returned response should have "<paramName>" and param value as config value

     Examples: 
      |paramName   |
      |AGENCY_ID   |
      |BRAND_ID    |
      |CAMPAIGN_ID |
      |FLIGHT_ID   |
      |PLACEMENT_ID|
      |AD_ID			 |
      |ASSET_ID    |	
      |ADV_CPM		 |
             
    				
      
      