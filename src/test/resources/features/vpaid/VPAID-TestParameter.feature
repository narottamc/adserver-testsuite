Feature: Verify Adserver Response for VPAID TAG with test parameters

  
  JIRA Ticket ID: CSCM-3608

  @init
  Scenario: Get VPAID AdTag for Verification
    Given I fetch a existing VPAID tag

  Scenario Outline: Verify API Response for VPIAD tag for Test parameter when user sets TEST_ENABLED value as true in Ad Tag URL
    Given User sets the Test enabled flag "<paramName>" in Ad tag URL
    When  User posts a get request to the updated VPAID tag for Test parameters
    Then  Returned response shoulde have status code as 200 and content-type as XML for Test param
    And   Returned response should have test params "<paramName>" same as config value
    
     Examples: 
      |paramName   |
      |TEST_ENABLED|
      
  Scenario Outline: Verify API Response for VPIAD tag for Test parameter when user do not set TEST_ENABLED value in AD tag URL
    When  User posts a get request to the updated VPAID tag for Test parameters in which Test Enabled flag not set
    Then  Returned response shoulde have status code as 200 and content-type as XML for Test param
    And   Returned response should have test params "<paramName>" and "<moddifiedValue>"
    
     Examples: 
      |paramName   |moddifiedValue|
      |TEST_ENABLED| false				|