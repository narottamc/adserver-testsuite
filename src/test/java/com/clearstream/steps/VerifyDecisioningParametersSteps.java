package com.clearstream.steps;

import java.util.ArrayList;
import java.util.Map;

import org.apache.log4j.Logger;

import com.adserver.mappers.VAST;
import com.adserver.models.AdTagParams.MetaParams;
import com.adserver.models.AdTagParams.VastQueryParams;
import com.adserver.models.AdTagsConfig.AdtagsTypes;
import com.clearstream.utils.AdParametersUtils;
import com.clearstream.utils.AdTagFactory;
import com.clearstream.utils.AdTagMapper;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import io.restassured.http.ContentType;

public class VerifyDecisioningParametersSteps {
	
	private static final Logger log = Logger.getLogger(VerifyDecisioningParametersSteps.class );
	
	private String adTagUrl = (String) Serenity.getCurrentSession().get("TAGURL");
	private Response responseAdTag;
	private Map<String, Object> AdMetaDataMap;
	private VAST objVAST;
	String UpdatedAdTag;
	String responsAdTagXMLString;
	
	@Given("^user updates the parameter \"([^\"]*)\" to \"([^\"]*)\"$")
	public void user_updates_the_parameter_to(String paramName, String modifiedValue) throws Exception {
		Map<String, Object> adTagUrlMap = AdTagMapper.mapGenerator(adTagUrl);
		AdMetaDataMap=(Map<String, Object>) adTagUrlMap.get(VastQueryParams.META.toString());
		AdMetaDataMap.put(MetaParams.class.getField(paramName).getName(), Boolean.valueOf(modifiedValue));
		UpdatedAdTag=AdTagMapper.updatedConfigAdTag(adTagUrl,adTagUrlMap);
		log.info("Updated VPIAD Tag : " + UpdatedAdTag);
	}
	
	
	
	
	@And("^user selects DoubleVerify\\(Fraud\\) as true$")
	public void user_selects_DoubleVerify_Fraud_as_true() throws Exception {
		Map<String, Object> adTagUrlMap = AdTagMapper.mapGenerator(UpdatedAdTag);
		AdMetaDataMap=(Map<String, Object>) adTagUrlMap.get(VastQueryParams.META.toString());
		AdMetaDataMap.put(MetaParams.class.getField("USE_DV_PPD").getName(), true);
		UpdatedAdTag=AdTagMapper.updatedConfigAdTag(UpdatedAdTag,adTagUrlMap);
		log.info("Updated VPIAD Tag : " + UpdatedAdTag);
		
	  
	}
	
	@Given("^user updates the parameter for IAS BrandSafety \"([^\"]*)\" and \"([^\"]*)\" to \"([^\"]*)\" and \"([^\"]*)\"$")
	public void user_updates_the_parameter_for_IAS_BrandSafety_and_to_and(String paramName, String paramName1, String modifiedValue, String modifiedValue1) throws Exception {
	   
		Map<String, Object> adTagUrlMap = AdTagMapper.mapGenerator(adTagUrl);
		AdMetaDataMap=(Map<String, Object>) adTagUrlMap.get(VastQueryParams.META.toString());
		AdMetaDataMap.put(MetaParams.class.getField(paramName).getName(), modifiedValue);
		AdMetaDataMap.put(MetaParams.class.getField(paramName1).getName(), modifiedValue1);
		UpdatedAdTag=AdTagMapper.updatedConfigAdTag(adTagUrl,adTagUrlMap);
		log.info("Updated VPIAD Tag : " + UpdatedAdTag);
		
	}

	

	@And("^user selects Integral Ad Science\\(IAS\\) Firewall as true$")
	public void user_selects_Integral_Ad_Science_IAS_Firewall_as_true() throws Exception {
		Map<String, Object> adTagUrlMap = AdTagMapper.mapGenerator(UpdatedAdTag);
		AdMetaDataMap=(Map<String, Object>) adTagUrlMap.get(VastQueryParams.META.toString());
		AdMetaDataMap.put(MetaParams.class.getField("USE_IAS").getName(), true);
		UpdatedAdTag=AdTagMapper.updatedConfigAdTag(UpdatedAdTag,adTagUrlMap);
		log.info("Updated VPIAD Tag : " + UpdatedAdTag);
	  
	}
	
	
	@When("^user post a get request to the updated VPAID tag$")
	public void user_post_a_get_request_to_the_updated_VPAID_tag() throws Exception {
		responseAdTag = io.restassured.RestAssured.given().contentType("application/xml;charset=UTF-8").when()
				.get(UpdatedAdTag);
	}
	
	@Then("^returned response shoulde have status code as (\\d+) and content-type as XML$")
	public void returnedResponseShouldeHaveStatusCodeAsAndContentTypeAsXML(int statusCode) throws Throwable {
			responseAdTag.then().statusCode(statusCode).contentType(ContentType.XML);
			log.info("Status Code is + " + responseAdTag.getStatusCode() + " And Content Type is : " + responseAdTag.getContentType());
			responsAdTagXMLString = responseAdTag.then().contentType(ContentType.XML).extract().response()
					.asString();
			log.info("Ad Tag URL Response : " + responsAdTagXMLString);
	}
	
	@And("^returned response should have \"([^\"]*)\" param value as \"([^\"]*)\"$")
	public void returned_response_should_have_param_value_as(String paramName, String modifiedValue) throws Exception {
		objVAST = AdTagFactory.getInstance(responsAdTagXMLString).getVast();
		AdParametersUtils.verifyDecisioningAdParameter(objVAST, paramName, Boolean.valueOf(modifiedValue));
	}

	

	@Then("^returned response should have \"([^\"]*)\" and \"([^\"]*)\"  param value as \"([^\"]*)\" and \"([^\"]*)\" for IAS BrandSafety$")
	public void returned_response_should_have_and_param_value_as_and_for_IAS_BrandSafety(String paramName, String paramName1, String modifiedValue, String modifiedValue1) throws Exception {
	
		objVAST = AdTagFactory.getInstance(responsAdTagXMLString).getVast();
		AdParametersUtils.verifyDecisioningAdParameter(objVAST,paramName,modifiedValue);
		AdParametersUtils.verifyDecisioningAdParameter(objVAST,paramName1,modifiedValue1);
	}
	
	
	
	
	

}
