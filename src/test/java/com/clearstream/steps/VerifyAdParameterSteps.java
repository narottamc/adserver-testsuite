package com.clearstream.steps;

import java.util.Map;

import org.apache.log4j.Logger;

import com.adserver.mappers.VAST;
import com.adserver.models.ConfigAssetsParms;
import com.adserver.models.AdTagParams.VastQueryParams;
import com.clearstream.utils.AdParametersUtils;
import com.clearstream.utils.AdTagFactory;
import com.clearstream.utils.AdTagMapper;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;

public class VerifyAdParameterSteps {
	
	private static final Logger log = Logger.getLogger(VerifyAdParameterSteps.class);
	
	private String adTagUrl = (String) Serenity.getCurrentSession().get("TAGURL");
	private Response responseAdTag;
	private VAST objVAST;
	String paramValue;
	
	String responsAdTagXMLString;
	
	@SuppressWarnings("unused")
	@Given("^User gets the ad tag url \"([^\"]*)\"from config data$")
	public void user_gets_the_ad_tag_url_from_config_data(String paramName) throws Exception {
		Map<String, Object> adTagUrlMap = AdTagMapper.mapGenerator(adTagUrl);
		paramValue=(String) AdTagMapper.listmapGenerator(adTagUrl, VastQueryParams.ASSETS.toString(), ConfigAssetsParms.class.getField(paramName).getName());
		 log.info("Ad VPIAD Tag : " + adTagUrl);
	}

	@When("^User post a get request to the updated VPAID tag for Ad$")
	public void user_post_a_get_request_to_the_updated_VPAID_tag_for_Ad() throws Exception {
		responseAdTag = io.restassured.RestAssured.given().contentType("application/xml;charset=UTF-8").when()
				.get(adTagUrl);
		
	}

	@Then("^Returned response shoulde have status code as (\\d+) and content-type as XML for Ad param$")
	public void returned_response_shoulde_have_status_code_as_and_content_type_as_XML_for_Ad_param(int statusCode) throws Exception {
	    
		responseAdTag.then().statusCode(statusCode).contentType(ContentType.XML);
		log.info("Status Code is + " + responseAdTag.getStatusCode() + " And Content Type is : " + responseAdTag.getContentType());
		responsAdTagXMLString = responseAdTag.then().contentType(ContentType.XML).extract().response()
				.asString();
		log.info("Ad Tag URL Response : " + responsAdTagXMLString);
	}

	@And("^Returned response should have ad params \"([^\"]*)\" same as config value$")
	public void returned_response_should_have_ad_params_same_as_config_value(String paramName) throws Exception {
		objVAST = AdTagFactory.getInstance(responsAdTagXMLString).getVast();
		AdParametersUtils.verifyAdParameter(objVAST, paramName, paramValue);
		
		
	}

}
