package com.clearstream.steps;

import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

public class SampleSteps {
	
	private RequestSpecification request;
	private Response response;
	//private Response json; 
	private ValidatableResponse json;
	
	//XmltoMAp(Sample VAST PArsing)

	@Given("^I have a \"([^\"]*)\" with ADTagType \"([^\"]*)\" and AD-Type \"([^\"]*)\"$")
	public void iHaveAWithADTagTypeAndADType(String arg1, String arg2, String arg3) throws Throwable {
		File file = new File("D:\\Project_Docs\\SMA\\VAST\\VPAID.xml");
		JAXBContext jaxbContext = JAXBContext.newInstance(com.adserver.mappers.VAST.class);
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		com.adserver.mappers.VAST vpaid = (com.adserver.mappers.VAST) unmarshaller.unmarshal(file);	
		System.out.println(vpaid.getAd().get(0).getInLine().getCreatives().getCreative().get(0).getLinear().getDuration());
	}

	@When("^I call \"([^\"]*)\" with AdTagType \"([^\"]*)\" and Ad-Type \"([^\"]*)\"$")
	public void iCallWithAdTagTypeAndAdType(String arg1, String arg2, String arg3) throws Throwable {
		File file = new File("D:\\Project_Docs\\SMA\\VAST\\VAST.xml");
		JAXBContext jaxbContext = JAXBContext.newInstance(com.adserver.mappers.VAST.class);
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		com.adserver.mappers.VAST vast = (com.adserver.mappers.VAST) unmarshaller.unmarshal(file);     
		System.out.println(vast.getAd().get(0).getWrapper().getCreatives().getCreative().get(0).getLinear().getTrackingEvents().getTracking().get(0).getValue());
	}

	@Then("^Status Code should display as \"([^\"]*)\"$")
	public void statusCodeShouldDisplayAs(String arg1) throws Throwable {
		System.out.println("TEST");
	}

	@Then("^Response should contain Ad-Type as \"([^\"]*)\"$")
	public void response_should_contain_Ad_Type_as(String arg1) throws Exception {
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("TEST");
	}
}
