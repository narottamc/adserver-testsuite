package com.clearstream.steps;

import java.util.Map;

import org.apache.log4j.Logger;

import com.adserver.mappers.VAST;
import com.adserver.models.AdTagParams.VastQueryParams;
import com.clearstream.utils.AdParametersUtils;
import com.clearstream.utils.AdTagFactory;
import com.clearstream.utils.AdTagMapper;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;

public class VerifyTestParameterSteps {
	
private static final Logger log = Logger.getLogger(VerifyTestParameterSteps.class );
	
	private String adTagUrl = (String) Serenity.getCurrentSession().get("TAGURL");
	private Response responseAdTag;
	private VAST objVAST;
	boolean paramValue;
	String responsAdTagXMLString;
	private String UpdatedAdTagURL;
	
	@Given("^User sets the Test enabled flag \"([^\"]*)\" in Ad tag URL$")
	public void user_sets_the_Test_enabled_flag_in_Ad_tag_URL(String paramName) throws Exception {
		   UpdatedAdTagURL=AdTagMapper.testEnabledAdTag(adTagUrl);
		  Map<String, Object> adTagUrlMap = AdTagMapper.mapAdparams(UpdatedAdTagURL);
			log.info("Ad VPIAD Tag : " + UpdatedAdTagURL);
			String TestValue= (String) adTagUrlMap.get(VastQueryParams.class.getField(paramName).getName());
			paramValue=Boolean.valueOf(TestValue);
			log.info("Test Value : " + paramValue);
		
	}
	
	@When("^User posts a get request to the updated VPAID tag for Test parameters$")
	public void user_posts_a_get_request_to_the_updated_VPAID_tag_for_Test_parameters() throws Exception {
		
		responseAdTag = io.restassured.RestAssured.given().contentType("application/xml;charset=UTF-8").when()
				.get(UpdatedAdTagURL);
	}

	@Then("^Returned response shoulde have status code as (\\d+) and content-type as XML for Test param$")
	public void returned_response_shoulde_have_status_code_as_and_content_type_as_XML_for_Test_param(int statusCode) throws Exception {
		responseAdTag.then().statusCode(statusCode).contentType(ContentType.XML);
		log.info("Status Code is + " + responseAdTag.getStatusCode() + " And Content Type is : " + responseAdTag.getContentType());
		responsAdTagXMLString = responseAdTag.then().contentType(ContentType.XML).extract().response()
				.asString();
		log.info("Ad Tag URL Response : " + responsAdTagXMLString);

	}
	
	
	@When("^User posts a get request to the updated VPAID tag for Test parameters in which Test Enabled flag not set$")
	public void user_posts_a_get_request_to_the_updated_VPAID_tag_for_Test_parameters_in_which_Test_Enabled_flag_not_set() throws Exception {
		responseAdTag = io.restassured.RestAssured.given().contentType("application/xml;charset=UTF-8").when()
				.get(adTagUrl);
	}


	@Then("^Returned response should have test params \"([^\"]*)\" and \"([^\"]*)\"$")
	public void returned_response_should_have_test_params_and(String paramName, String moddifiedValue) throws Exception {
		objVAST = AdTagFactory.getInstance(responsAdTagXMLString).getVast();
		AdParametersUtils.verifyTestAdParameter(objVAST, paramName, Boolean.valueOf(moddifiedValue));
	}
	

	@And("^Returned response should have test params \"([^\"]*)\" same as config value$")
	public void returned_response_should_have_test_params_same_as_config_value(String paramName) throws Exception {
		objVAST = AdTagFactory.getInstance(responsAdTagXMLString).getVast();
		AdParametersUtils.verifyTestAdParameter(objVAST, paramName, paramValue);
	}
	
	

}
