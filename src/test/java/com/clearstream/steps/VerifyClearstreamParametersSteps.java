package com.clearstream.steps;

import java.util.Map;

import org.apache.log4j.Logger;

import com.adserver.mappers.VAST;
import com.adserver.models.ConfigAssetsParms;
import com.adserver.models.AdTagParams.MetaParams;
import com.adserver.models.AdTagParams.VastQueryParams;
import com.clearstream.utils.AdParametersUtils;
import com.clearstream.utils.AdTagFactory;
import com.clearstream.utils.AdTagMapper;


import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;

public class VerifyClearstreamParametersSteps {
	
	
private static final Logger log = Logger.getLogger(VerifyClearstreamParametersSteps.class );
	
	private String adTagUrl = (String) Serenity.getCurrentSession().get("TAGURL");
	private Response responseAdTag;
	private Map<String, Object> AdMetaDataMap;
	private VAST objVAST;
	public int paramValue;
	public double paramValueADVCPM;
	String UpdatedAdTag;
	String responsAdTagXMLString;
	
	
	@SuppressWarnings("unchecked")
	@Given("^user gets the clearstream par \"([^\"]*)\" from config data$")
	public void user_gets_the_clearstream_par_from_config_data(String paramName) throws Exception {
	    if(!paramName.equals("ASSET_ID") & (!paramName.equals("ADV_CPM"))){
		Map<String, Object> adTagUrlMap = AdTagMapper.mapGenerator(adTagUrl);
		AdMetaDataMap=(Map<String, Object>) adTagUrlMap.get(VastQueryParams.META.toString());
		paramValue= (int) AdMetaDataMap.get(MetaParams.class.getField(paramName).getName());
		
	    }
	    else if(paramName.equals("ADV_CPM"))
	    {
	    	Map<String, Object> adTagUrlMap = AdTagMapper.mapGenerator(adTagUrl);
			AdMetaDataMap=(Map<String, Object>) adTagUrlMap.get(VastQueryParams.META.toString());
			paramValueADVCPM= (double) AdMetaDataMap.get(MetaParams.class.getField(paramName).getName());
			
	    }
	    else
	    {
	    	Map<String, Object> adTagUrlMap = AdTagMapper.mapGenerator(adTagUrl);
			paramValue=(int) AdTagMapper.listmapGenerator(adTagUrl, VastQueryParams.ASSETS.toString(), ConfigAssetsParms.ASSET_ID);
	    }
	    log.info("Ad VPIAD Tag : " + adTagUrl);
	}

	@When("^user post a get request to the updated VPAID tag for clearstream parameters$")
	public void user_post_a_get_request_to_the_updated_VPAID_tag_for_clearstream_parameters() throws Exception {
		responseAdTag = io.restassured.RestAssured.given().contentType("application/xml;charset=UTF-8").when()
				.get(adTagUrl);
	}
	
	@Then("^returned response shoulde have status code as (\\d+) and content-type as XML for clearstream param$")
	public void returned_response_shoulde_have_status_code_as_and_content_type_as_XML_for_clearstream_param(int statusCode) throws Exception {
		responseAdTag.then().statusCode(statusCode).contentType(ContentType.XML);
		log.info("Status Code is + " + responseAdTag.getStatusCode() + " And Content Type is : " + responseAdTag.getContentType());
		responsAdTagXMLString = responseAdTag.then().contentType(ContentType.XML).extract().response()
				.asString();
		log.info("Ad Tag URL Response : " + responsAdTagXMLString);
		
	}
	
	@Then("^returned response should have \"([^\"]*)\" and param value as config value$")
	public void returned_response_should_have_and_param_value_as_config_value(String paramName) throws Exception {
		objVAST = AdTagFactory.getInstance(responsAdTagXMLString).getVast();
		if(paramName.equals("ADV_CPM"))
		{
			AdParametersUtils.verifyClearstreamParameterAdParameter(objVAST, paramName, paramValueADVCPM);
		}
		else 
		{
		AdParametersUtils.verifyClearstreamParameterAdParameter(objVAST, paramName, paramValue);
		}
	}
	
	
}

