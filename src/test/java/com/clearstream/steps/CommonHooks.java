package com.clearstream.steps;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;

import org.apache.log4j.Logger;
import org.ini4j.InvalidFileFormatException;

import com.adserver.models.AdTagsConfig.AdtagsTypes;
import com.clearstream.utils.AdTagGenerator;
import com.clearstream.utils.CreatePostGresConnection;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import net.serenitybdd.core.Serenity;

public class CommonHooks {

	private static final Logger log = Logger.getLogger(CommonHooks.class);

	@Before("@init")
	public void setup() throws InvalidFileFormatException, FileNotFoundException, IOException {
		CreatePostGresConnection connection = new CreatePostGresConnection();
		connection.setConnection();
		Connection conn_ = connection.getConnection();
		Serenity.getCurrentSession().put("dbConnObject", conn_);
	}

	@Given("^I fetch a existing VPAID tag$")
	public void i_fetch_a_existing_VPAID_tag() throws Exception {
		AdTagGenerator adTagGenerator = new AdTagGenerator();
		String adTagUrl = adTagGenerator.getVideoAlchemyAdtag(AdtagsTypes.VPAID_TYPE, AdtagsTypes.VIDEO_AD,
				(Connection) Serenity.getCurrentSession().get("dbConnObject"));
		log.info("Before Hook : " + adTagUrl);
		Serenity.getCurrentSession().put("TAGURL", adTagUrl);
	}

	// @Before("@VPAID")
	// public void getVPAIDTag() throws InvalidFileFormatException,
	// FileNotFoundException, IOException{
	// AdTagGenerator adTagGenerator= new AdTagGenerator();
	// String adTagUrl =
	// adTagGenerator.getVideoAlchemyAdtag(AdtagsTypes.VPAID_TYPE,
	// AdtagsTypes.VIDEO_AD, (Connection)
	// Serenity.getCurrentSession().get("dbConnObject"));
	// System.out.println("Before Hook : "+ adTagUrl);
	// Serenity.getCurrentSession().put("TAGURL", adTagUrl);
	// }
	

}
