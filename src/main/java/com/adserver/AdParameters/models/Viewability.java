package com.adserver.AdParameters.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Viewability {

@SerializedName("min")
@Expose
private Integer min;
@SerializedName("minPlayerWidth")
@Expose
private Integer minPlayerWidth;
@SerializedName("scrollSecsStart")
@Expose
private Integer scrollSecsStart;
@SerializedName("scrollSecsEnd")
@Expose
private Integer scrollSecsEnd;
@SerializedName("startOnlyInFocus")
@Expose
private Boolean startOnlyInFocus;
@SerializedName("startOnlyInView")
@Expose
private Boolean startOnlyInView;

public Integer getMin() {
return min;
}

public void setMin(Integer min) {
this.min = min;
}

public Integer getMinPlayerWidth() {
return minPlayerWidth;
}

public void setMinPlayerWidth(Integer minPlayerWidth) {
this.minPlayerWidth = minPlayerWidth;
}

public Integer getScrollSecsStart() {
return scrollSecsStart;
}

public void setScrollSecsStart(Integer scrollSecsStart) {
this.scrollSecsStart = scrollSecsStart;
}

public Integer getScrollSecsEnd() {
return scrollSecsEnd;
}

public void setScrollSecsEnd(Integer scrollSecsEnd) {
this.scrollSecsEnd = scrollSecsEnd;
}

public Boolean getStartOnlyInFocus() {
return startOnlyInFocus;
}

public void setStartOnlyInFocus(Boolean startOnlyInFocus) {
this.startOnlyInFocus = startOnlyInFocus;
}

public Boolean getStartOnlyInView() {
return startOnlyInView;
}

public void setStartOnlyInView(Boolean startOnlyInView) {
this.startOnlyInView = startOnlyInView;
}


}
