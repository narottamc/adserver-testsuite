package com.adserver.AdParameters.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Click {
	@SerializedName("url")
	@Expose
	private String url;
	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("playerHandles")
	@Expose
	private Boolean playerHandles;

	public String getUrl() {
	return url;
	}

	public void setUrl(String url) {
	this.url = url;
	}

	public Integer getId() {
	return id;
	}

	public void setId(Integer id) {
	this.id = id;
	}

	public Boolean getPlayerHandles() {
	return playerHandles;
	}

	public void setPlayerHandles(Boolean playerHandles) {
	this.playerHandles = playerHandles;
	}

}
