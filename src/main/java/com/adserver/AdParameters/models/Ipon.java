package com.adserver.AdParameters.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ipon {
	@SerializedName("SEGMENTS")
	@Expose
	private String sEGMENTS;
	@SerializedName("SITE_DOMAIN")
	@Expose
	private String sITEDOMAIN;

	public String getSEGMENTS() {
	return sEGMENTS;
	}

	public void setSEGMENTS(String sEGMENTS) {
	this.sEGMENTS = sEGMENTS;
	}

	public String getSITEDOMAIN() {
	return sITEDOMAIN;
	}

	public void setSITEDOMAIN(String sITEDOMAIN) {
	this.sITEDOMAIN = sITEDOMAIN;
	}

}
