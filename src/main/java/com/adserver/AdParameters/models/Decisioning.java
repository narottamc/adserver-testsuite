package com.adserver.AdParameters.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Decisioning {
	@SerializedName("useDV")
	@Expose
	private Boolean USE_DV;
	@SerializedName("useDVPPD")
	@Expose
	private Boolean USE_DV_PPD;
	@SerializedName("dvPPDTimeout")
	@Expose
	private Boolean DV_PPD_TIMEOUT;
	@SerializedName("useIAB")
	@Expose
	private Boolean USE_IAB;
	@SerializedName("useIAS")
	@Expose
	private Boolean USE_IAS;
	@SerializedName("iasTimeout")
	@Expose
	private Boolean IAS_TIMEOUT;
	@SerializedName("iasAdvEntityId")
	@Expose
	private String IAS_ADV_ENTITY_ID;
	@SerializedName("iasPubEntityId")
	@Expose
	private String IAS_PUB_ENTITY_ID;
	@SerializedName("useAdChoices")
	@Expose
	private Boolean USE_AD_CHOICES;
	@SerializedName("geoFirewallExit")
	@Expose
	private Boolean geoFirewallExit;
	@SerializedName("isIABBrowser")
	@Expose
	private Boolean isIABBrowser;
	@SerializedName("isIABSpider")
	@Expose
	private Boolean isIABSpider;



	public Boolean getIAS_TIMEOUT() {
		return IAS_TIMEOUT;
	}

	public void setIAS_TIMEOUT(Boolean iAS_TIMEOUT) {
		IAS_TIMEOUT = iAS_TIMEOUT;
	}


	public String getIAS_ADV_ENTITY_ID() {
		return IAS_ADV_ENTITY_ID;
	}

	public void setIAS_ADV_ENTITY_ID(String iAS_ADV_ENTITY_ID) {
		IAS_ADV_ENTITY_ID = iAS_ADV_ENTITY_ID;
	}

	public String getIAS_PUB_ENTITY_ID() {
		return IAS_PUB_ENTITY_ID;
	}

	public void setIAS_PUB_ENTITY_ID(String iAS_PUB_ENTITY_ID) {
		IAS_PUB_ENTITY_ID = iAS_PUB_ENTITY_ID;
	}


	public Boolean getUSE_DV() {
		return USE_DV;
	}

	public void setUSE_DV(Boolean uSE_DV) {
		USE_DV = uSE_DV;
	}

	public Boolean getUSE_DV_PPD() {
		return USE_DV_PPD;
	}

	public void setUSE_DV_PPD(Boolean uSE_DV_PPD) {
		USE_DV_PPD = uSE_DV_PPD;
	}
	
	
	public Boolean getDV_PPD_TIMEOUT() {
		return DV_PPD_TIMEOUT;
	}

	public void setDV_PPD_TIMEOUT(Boolean uSE_DV_PPD_TIMEOUT) {
		DV_PPD_TIMEOUT = uSE_DV_PPD_TIMEOUT;
	}
	
	
	public Boolean getUSE_IAB() {
		return USE_IAB;
	}

	public void setUSE_IAB(Boolean uSE_IAB) {
		USE_IAB = uSE_IAB;
	}

	public Boolean getUSE_IAS() {
		return USE_IAS;
	}

	public void setUSE_IAS(Boolean uSE_IAS) {
		USE_IAS = uSE_IAS;
	}

	public Boolean getUSE_AD_CHOICES() {
		return USE_AD_CHOICES;
	}

	public void setUSE_AD_CHOICES(Boolean uSE_AD_CHOICES) {
		USE_AD_CHOICES = uSE_AD_CHOICES;
	}

	public Boolean getGeoFirewallExit() {
	return geoFirewallExit;
	}

	public void setGeoFirewallExit(Boolean geoFirewallExit) {
	this.geoFirewallExit = geoFirewallExit;
	}

	public Boolean getIsIABBrowser() {
	return isIABBrowser;
	}

	public void setIsIABBrowser(Boolean isIABBrowser) {
	this.isIABBrowser = isIABBrowser;
	}

	public Boolean getIsIABSpider() {
	return isIABSpider;
	}

	public void setIsIABSpider(Boolean isIABSpider) {
	this.isIABSpider = isIABSpider;
	}

}
