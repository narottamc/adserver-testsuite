package com.adserver.AdParameters.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ad {
	@SerializedName("tag")
	@Expose
	private String EXTERNAL_URL;
	@SerializedName("VPAIDWrapper")
	@Expose
	private Boolean vPAIDWrapper;

	public String getEXTERNAL_URL() {
		return EXTERNAL_URL;
	}

	public void setEXTERNAL_URL(String eXTERNAL_URL) {
		EXTERNAL_URL = eXTERNAL_URL;
	}

	public Boolean getVPAIDWrapper() {
		return vPAIDWrapper;
	}

	public void setVPAIDWrapper(Boolean vPAIDWrapper) {
		this.vPAIDWrapper = vPAIDWrapper;
	}

}
