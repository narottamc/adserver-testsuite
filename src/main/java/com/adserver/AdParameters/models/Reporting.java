package com.adserver.AdParameters.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Reporting {
	@SerializedName("enabled")
	@Expose
	private Boolean enabled;
	@SerializedName("orderid")
	@Expose
	private String orderid;
	@SerializedName("ipon")
	@Expose
	private Ipon ipon;
	@SerializedName("liverail")
	@Expose
	private Liverail liverail;
	@SerializedName("clearstream")
	@Expose
	private ClearStream clearstream;

	public Boolean getEnabled() {
	return enabled;
	}

	public void setEnabled(Boolean enabled) {
	this.enabled = enabled;
	}

	public String getOrderid() {
	return orderid;
	}

	public void setOrderid(String orderid) {
	this.orderid = orderid;
	}

	public Ipon getIpon() {
	return ipon;
	}

	public void setIpon(Ipon ipon) {
	this.ipon = ipon;
	}

	public Liverail getLiverail() {
	return liverail;
	}

	public void setLiverail(Liverail liverail) {
	this.liverail = liverail;
	}

	public ClearStream getClearstream() {
	return clearstream;
	}

	public void setClearstream(ClearStream clearstream) {
	this.clearstream = clearstream;
	}

}
