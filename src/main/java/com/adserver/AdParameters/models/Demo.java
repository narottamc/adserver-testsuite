package com.adserver.AdParameters.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Demo {
	@SerializedName("enabled")
	@Expose
	private Boolean enabled;

	public Boolean getEnabled() {
	return enabled;
	}

	public void setEnabled(Boolean enabled) {
	this.enabled = enabled;
	}

}
