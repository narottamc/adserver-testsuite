package com.adserver.AdParameters.models;

import java.lang.reflect.Field;

import com.google.gson.FieldNamingPolicy;

public class FieldNamingStrategy implements com.google.gson.FieldNamingStrategy {

	@Override
	public String translateName(Field field) {
		// TODO Auto-generated method stub
		String fieldName = FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES.translateName(field);
		return fieldName;
	}

}
