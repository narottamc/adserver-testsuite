package com.adserver.AdParameters.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RootAdParameters {
	@SerializedName("adType")
	@Expose
	private String AD_TYPE;
	@SerializedName("ad")
	@Expose
	private Ad ad;
	@SerializedName("click")
	@Expose
	private Click click;
	@SerializedName("decisioning")
	@Expose
	private Decisioning decisioning;
	@SerializedName("demo")
	@Expose
	private Demo demo;
	@SerializedName("test")
	@Expose
	private Test TEST_ENABLED;
	@SerializedName("geo")
	@Expose
	private Geo GEO;
	@SerializedName("reporting")
	@Expose
	private Reporting reporting;
	@SerializedName("viewability")
	@Expose
	private Viewability viewability;
	@SerializedName("update")
	@Expose
	private Update update;
	@SerializedName("queryParams")
	@Expose
	private QueryParams queryParams;

	

	public String getAD_TYPE() {
		return AD_TYPE;
	}

	public void setAD_TYPE(String aD_TYPE) {
		AD_TYPE = aD_TYPE;
	}

	public Ad getAd() {
	return ad;
	}

	public void setAd(Ad ad) {
	this.ad = ad;
	}

	public Click getClick() {
	return click;
	}

	public void setClick(Click click) {
	this.click = click;
	}

	public Decisioning getDecisioning() {
	return decisioning;
	}

	public void setDecisioning(Decisioning decisioning) {
	this.decisioning = decisioning;
	}

	public Demo getDemo() {
	return demo;
	}

	public void setDemo(Demo demo) {
	this.demo = demo;
	}



	public Test getTEST_ENABLED() {
		return TEST_ENABLED;
	}

	public void setTEST_ENABLED(Test tEST_ENABLED) {
		TEST_ENABLED = tEST_ENABLED;
	}

	public Geo getGEO() {
		return GEO;
	}

	public void setGEO(Geo gEO) {
		GEO = gEO;
	}

	public Reporting getReporting() {
	return reporting;
	}

	public void setReporting(Reporting reporting) {
	this.reporting = reporting;
	}

	public Viewability getViewability() {
	return viewability;
	}

	public void setViewability(Viewability viewability) {
	this.viewability = viewability;
	}

	public Update getUpdate() {
	return update;
	}

	public void setUpdate(Update update) {
	this.update = update;
	}

	public QueryParams getQueryParams() {
	return queryParams;
	}

	public void setQueryParams(QueryParams queryParams) {
	this.queryParams = queryParams;
	}

}
