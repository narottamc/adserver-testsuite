package com.adserver.AdParameters.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClearStream {
	@SerializedName("AGENCY_ID")
	@Expose
	private Integer AGENCY_ID;
	@SerializedName("ASSET_ID")
	@Expose
	private Integer ASSET_ID;
	@SerializedName("BRAND_ID")
	@Expose
	private Integer BRAND_ID;
	@SerializedName("CAMPAIGN_ID")
	@Expose
	private Integer CAMPAIGN_ID;
	@SerializedName("FLIGHT_ID")
	@Expose
	private Integer FLIGHT_ID;
	@SerializedName("PLACEMENT_ID")
	@Expose
	private Integer PLACEMENT_ID;
	@SerializedName("ADV_CPM")
	@Expose
	private Double ADV_CPM;
	@SerializedName("AD_ID")
	@Expose
	private Integer AD_ID;
	
	public Integer getAGENCY_ID() {
		return AGENCY_ID;
	}
	public void setAGENCY_ID(Integer aGENCY_ID) {
		AGENCY_ID = aGENCY_ID;
	}
	public Integer getASSET_ID() {
		return ASSET_ID;
	}
	public void setASSET_ID(Integer aSSET_ID) {
		ASSET_ID = aSSET_ID;
	}
	public Integer getBRAND_ID() {
		return BRAND_ID;
	}
	public void setBRAND_ID(Integer bRAND_ID) {
		BRAND_ID = bRAND_ID;
	}
	public Integer getCAMPAIGN_ID() {
		return CAMPAIGN_ID;
	}
	public void setCAMPAIGN_ID(Integer cAMPAIGN_ID) {
		CAMPAIGN_ID = cAMPAIGN_ID;
	}
	public Integer getFLIGHT_ID() {
		return FLIGHT_ID;
	}
	public void setFLIGHT_ID(Integer fLIGHT_ID) {
		FLIGHT_ID = fLIGHT_ID;
	}
	public Integer getPLACEMENT_ID() {
		return PLACEMENT_ID;
	}
	public void setPLACEMENT_ID(Integer pLACEMENT_ID) {
		PLACEMENT_ID = pLACEMENT_ID;
	}
	public Double getADV_CPM() {
		return ADV_CPM;
	}
	public void setADV_CPM(Double aDV_CPM) {
		ADV_CPM = aDV_CPM;
	}
	public Integer getAD_ID() {
		return AD_ID;
	}
	public void setAD_ID(Integer aD_ID) {
		AD_ID = aD_ID;
	}
	

}
