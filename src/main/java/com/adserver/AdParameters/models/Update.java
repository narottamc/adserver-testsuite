package com.adserver.AdParameters.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Update {
	@SerializedName("period")
	@Expose
	private Integer period;

	public Integer getPeriod() {
	return period;
	}

	public void setPeriod(Integer period) {
	this.period = period;
	}

}
