package com.adserver.models;


public class AdTagsConfig {
	
	/**
	 * Enum for Ad tag types and ads
	 * @author Clearstream
	 *
	 */
	public enum AdtagsTypes {
		
		VAST_TYPE("VAST"),
		VPAID_TYPE("VPAID"),
		VIDEO_AD("VIDEO"),
		HTML_AD("HTML"),
		VIDEO_CONTENT("video"),
		SURVEY_CONTENT("survey"),
		DISPLAY_CONTENT("display");
		
		
		String lable;

		private AdtagsTypes(String lable) {
			this.lable = lable;
		}

		public String getLabel() {
			return this.lable;
		}

		public String toString() {
			return this.lable;
		}
	}
	
	
	public enum PGProperties
	{
		HOST_ADDRESS("jdbc:postgresql://openstream-database-development.cmg2oeoqlpk8.us-east-1.rds.amazonaws.com/"),
		DATABASE_NAME("openstream"),
		UNAME("openstream"),
		PASSWORD("Clearstream1");
		

		String label;
		
		private PGProperties(String label)
		{
			this.label = label;
		}

		public String getLabel()
		{
			return this.label;
		}

		public String toString()
		{
			return this.label;
		}
	}
}
