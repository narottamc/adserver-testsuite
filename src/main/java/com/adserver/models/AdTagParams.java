package com.adserver.models;

public class AdTagParams {
	
	/**
	 * Enum for META params
	 * @author Clearstream
	 *
	 */

public enum MetaParams {
		
		AGENCY_ID("AGENCY_ID"),
		BRAND_ID("BRAND_ID"),
		CAMPAIGN_ID("CAMPAIGN_ID"),
		PLACEMENT_ID("PLACEMENT_ID"),
		FLIGHT_ID("FLIGHT_ID"),
		AD_ID("AD_ID"),
		USE_DV("USE_DV"),
		USE_DV_PPD("USE_DV_PPD"),
		DV_PPD_TIMEOUT("DV_PPD_TIMEOUT"),
		USE_IAB("USE_IAB"),
		USE_IAS("USE_IAS"),
		ADV_CPM("ADV_CPM"),
		USE_WHITEOPS("USE_WHITEOPS"),
		USE_AD_CHOICES("USE_AD_CHOICES"),
		CAMPAIGN_TYPE_ID("CAMPAIGN_TYPE_ID"),
		REPLACEABLE("REPLACEABLE"),
		IAS_TIMEOUT("IAS_TIMEOUT"),
		IAS_ADV_ENTITY_ID("IAS_ADV_ENTITY_ID"),
		IAS_PUB_ENTITY_ID("IAS_PUB_ENTITY_ID"),
		MIN_VIEWABILITY("MIN_VIEWABILITY"),
		;
		
		String lable;

		private MetaParams(String lable) {
			this.lable = lable;
		}

		public String getLabel() {
			return this.lable;
		}

		public String toString() {
			return this.lable;
		}
	}
	
/**
 * Enum for Ad tags params
 * @author Clearstream
 *
 */

	public enum VastQueryParams {
		CONFIG("CONFIG"),
		META("META"),
		ASSETS("ASSETS"),
		ORDERID("ORDERID"), 
		TEST_ENABLED("TEST_ENABLED"),
		GEO("GEO"),
		AD_TYPE("AD_TYPE"),
		SSP("SSP"),
		EVENT_TYPE("event_type");
		String lable;

		private VastQueryParams(String lable) {
			this.lable = lable;
		}

		public String getLabel() {
			return this.lable;
		}

		public String paramName() {
			return this.lable;
		}
	}
	
	/**
	 * Enum for Asset list params
	 * @author Clearstream
	 *
	 */
	
	public enum ConfigAssetsParms {
		
		ORDERID("ORDERID"),
		ASSET_ID("ASSET_ID"),
		EXTERNAL_URL("EXTERNAL_URL"),
		ALLOCATION_PERCENT("ALLOCATION_PERCENT"),
		AD_SPEC("AD_SPEC");
		
		
		String lable;

		private ConfigAssetsParms(String lable) {
			this.lable = lable;
		}

		public String getLabel() {
			return this.lable;
		}

		public String toString() {
			return this.lable;
		}
	}
	
}
