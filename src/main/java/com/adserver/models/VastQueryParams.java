package com.adserver.models;

import java.util.HashMap;
import java.util.Map;

public enum VastQueryParams
{
	USE_IAB("USE_IAB", "TRUE"),
	GEO("GEO", "TRUE"),
	BID_CPM("BID_CPM", "7.1614"),
	PUBLISHER_ID("PUBLISHER_ID","bidswitch_optimatic_91606104"),
	SELLER_NETWORK("SELLER_NETWORK", "bidswitch_optimatic"),
	SITE_DOMAIN("SITE_DOMAIN", "flyingmag.com"),
	PAGE_DOMAIN("PAGE_DOMAIN", "flyingmag.com"),
	DEVICE_IP("DEVICE_IP", "70.59.83.35"),
	COOKIE_ID("COOKIE_ID", "8715f446-8bbf-4bc6-95d0-e6e9d338e038"),
	IDFA("IDFA", "8715f446-8bbf-4bc6-95d0-e6e9d338e038"),
	USER_AGENT("USER_AGENT", "Mozilla%2F5.0+%28Windows+NT+6.1%3B+WOW64%29+AppleWebKit%2F537.36+%28KHTML%2C+like+Gecko%29+Chrome%2F63.0.3239.108+Safari%2F537.36"),
	BID_REQ_ID("BID_REQ_ID", "1111103093e23_a69a1ef7-b2be-48b4-ac5f-91fc550ba115"),
	GEO_REGION("GEO_REGION", "MN"),
	GEO_COUNTRY("GEO_COUNTRY", "US"),
	GEO_ZIP("GEO_ZIP", "T2G"),
	GEO_DMA("GEO_DMA", "613"),
	DEVICE_TYPE("DEVICE_TYPE", "PC"),
	OS_TYPE("OS_TYPE", "Windows"),
	OS_VERSION("OS_VERSION", "Windows_7"),
	LANGUAGE("LANGUAGE", "en"),
	API_FRAMEWORK("API_FRAMEWORK", "1,2"),
	PROTOCOL("PROTOCOL", "2,5"),
	SSP("SSP", "bidswitch"),
	USE_DV("USE_DV", "TRUE"),
	REPORTING_IPON_IMPRESSION_ID("REPORTING_IPON_IMPRESSION_ID", "e2eeca98-8923-486d-b636-7e3559914f5a"),
	SEG_IPON("SEG_IPON", "352"),
	SEG_LRMP("SEG_LRMP", "1001297619"),
	SEG_LOTA("SEG_LOTA", "773"),
	AAID("AAID", "ec524ded-79e7-4303-911c-8c2e0a495043"),
	CAT("CAT", "IAB24"),
	APP_BUNDLE("APP_BUNDLE", "com.picsart.studio"),
	WINSEG("WINSEG", "lotame:385519");

	String qsAttribName, qsAttribValue;
	
	public static Map<VastQueryParams,String> getQSAttributes(){
		Map<VastQueryParams,String> qSAttributes = new HashMap<VastQueryParams, String>();				
		
		for (VastQueryParams qsAttrib : VastQueryParams.values())
		{
			qSAttributes.put(qsAttrib, qsAttrib.getqsAttribValue());
		}
		return qSAttributes;
			
	}
	
	private VastQueryParams(String qsAttribName, String qsAttribValue)
	{
		this.qsAttribName = qsAttribName;
		this.qsAttribValue = qsAttribValue;
		
	}

	public String getqsAttribName()
	{
		return this.qsAttribName;
	}

	public String getqsAttribValue()
	{
		return this.qsAttribValue;
	}
}
