package com.adserver.models;

import java.util.HashMap;

/**
 * @author Clearstream
 */
public enum MetaParms
{
	AGENCY_ID("AGENCY_ID","100"),
	BRAND_ID("BRAND_ID","100"),
	CAMPAIGN_ID("CAMPAIGN_ID","2023"),
	PLACEMENT_ID("PLACEMENT_ID","15000"),
	FLIGHT_ID("FLIGHT_ID","36000"),
	AD_ID("AD_ID","36000"),
	AD_TYPE("AD_TYPE","VIDEO"),
	USE_DV("USE_DV","false"),
	USE_DV_PPD("USE_DV_PPD","false"),
	USE_IAB("USE_IAB","false"),
	USE_IAS("USE_IAS","false"),
	ADV_CPM("ADV_CPM","25"),
	USE_WHITEOPS("USE_WHITEOPS","false"),
	USE_AD_CHOICES("USE_AD_CHOICES","false"),
	CAMPAIGN_TYPE_ID("CAMPAIGN_TYPE_ID","1"),
	REPLACEABLE("REPLACEABLE","false"),
	IAS_TIMEOUT("IAS_TIMEOUT","true"),
	IAS_ADV_ENTITY_ID("IAS_ADV_ENTITY_ID","96700"),
	IAS_PUB_ENTITY_ID("IAS_PUB_ENTITY_ID","17062117"),
	MIN_VIEWABILITY("MIN_VIEWABILITY","50"),
	;

	String label,defaultValue, configAttribName, configAttribValue;

	public static HashMap<String,String> getMetaAttributes(){
		HashMap<String,String> configAttributes = new HashMap<String, String>();				
		
		
		for (MetaParms qsAttrib : MetaParms.values())
		{
			configAttributes.put(qsAttrib.getMetaAttribName(), qsAttrib.getMetaAttribValue());
		}
		return configAttributes;
			
	}
	
	private MetaParms(String label,String defaultValue)
	{
		this.configAttribName = label;
		this.configAttribValue = defaultValue;
	}

	public String getMetaAttribName()
	{
		return this.configAttribName;
	}

	public String getMetaAttribValue()
	{
		return this.configAttribValue;
	}
}
