package com.adserver.models;

import java.util.HashMap;

/**
 * @author Clearstream
 */
public enum ConfigAssetsParms
{
	
	ORDERID("ORDERID","36000"),
	ASSET_ID("ASSET_ID","36000"),
	EXTERNAL_URL("EXTERNAL_URL","https://fw.adsafeprotected.com/vast/fwjsvid/st/156419/24343366/skeleton.js?includeFlash=false&originalVast=https://ad.doubleclick.net/ddm/pfadx/N2724.1302111CLEARSTREAM.TV/B20957501.218054426;sz=0x0;ord=[timestamp];dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;dcmt=text/xml"),
	ALLOCATION_PERCENT("ALLOCATION_PERCENT","100"),
	AD_SPEC("AD_SPEC","VPAID-JS");
	
	String label,defaultValue, configAttribName, configAttribValue;

	public static HashMap<String,String> getConfigAttributes(){
		HashMap<String,String> configAttributes = new HashMap<String, String>();				
		
		
		for (ConfigAssetsParms qsAttrib : ConfigAssetsParms.values())
		{
			configAttributes.put(qsAttrib.getConfigAttribName(), qsAttrib.getConfigAttribValue());
		}
		return configAttributes;
			
	}
	
	private ConfigAssetsParms(String label,String defaultValue)
	{
		this.configAttribName = label;
		this.configAttribValue = defaultValue;
	}

	public String getConfigAttribName()
	{
		return this.configAttribName;
	}

	public String getConfigAttribValue()
	{
		return this.configAttribValue;
	}
}
