package com.clearstream.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyFileReader {
	
	/**
	 * Description: Return properties file value
	 * @param propvalue
	 * @return
	 */
	public static String getProperties(String propvalue)
	{
 
	Properties prop = new Properties();
	InputStream input = null;
	String properties=null;

	try {
        String filepath=GlobalConstants.LOCAL_CONFIG_PROPERTY_FILENAME;
		input = new FileInputStream(filepath);


		prop.load(input);
		properties = prop.getProperty(propvalue);
		

	} catch (IOException ex) {
		ex.printStackTrace();
	} finally {
		if (input != null) {
			try {
				input.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	return properties;
	}
}



