package com.clearstream.utils;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.bcel.classfile.ConstantString;
import org.apache.log4j.Logger;
import org.ini4j.BasicMultiMap;
import org.ini4j.Ini;
import org.ini4j.InvalidFileFormatException;
import org.ini4j.Profile.Section;



public class CreatePostGresConnection {
	
	private static final Logger log = Logger.getLogger(CreatePostGresConnection.class );

	private Connection conn;

	public CreatePostGresConnection() {
	}

	/**
	 * Description: Set POSTGRES database connection
	 * 
	 * @throws IOException
	 * @throws FileNotFoundException
	 * @throws InvalidFileFormatException
	 */

	public void setConnection() throws InvalidFileFormatException, FileNotFoundException, IOException {
		Ini configIni = new Ini();
		configIni.load(new FileReader(GlobalConstants.LOCAL_CONFIG_PROPERTY_FILENAME));

		Ini.Section envValue = configIni.get(System.getProperty("env")) != null
				? configIni.get(System.getProperty("env"))
				: configIni.get(GlobalConstants.ENV_DEV);

		// String testParam = envValue.get("TEST_PARAM");
		// System.out.println(testParam);
		String connString = envValue.fetch("HOST_ADDRESS") + envValue.fetch("DATABASE_NAME");
		String uname = envValue.fetch("UNAME");
		String password = envValue.fetch("PASSWORD");

		try {
			this.conn = DriverManager.getConnection(connString, uname, password);
			this.conn.setAutoCommit(true);
			log.info("Postgre Connection : Connection Successfull");
		} catch (SQLException e) {
			log.info(e.getMessage());
		}
	}

	/**
	 * Description: Get POSTGRES database Connection
	 * 
	 * @return
	 */
	public Connection getConnection() {
		return this.conn;
	}

	/**
	 * Description: close POSTGRES database
	 */
	public void closeConnection(Connection conn) {
		try {
			conn.close();
			log.info("Postgre Connection : Connection Closed");
		} catch (SQLException e) {
			log.info(e.getMessage());
		}
	}

}
