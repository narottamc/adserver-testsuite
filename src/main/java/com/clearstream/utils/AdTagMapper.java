package com.clearstream.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.adserver.models.ConfigAssetsParms;
import com.adserver.models.AdTagParams.VastQueryParams;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import jline.internal.Log;




public class AdTagMapper {

	
	/**
	 * Description: Return map from AD tag URL
	 * @param urlWithParams
	 * @param Ad_Type
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, Object>  mapGenerator(String vastURL) throws JsonParseException, JsonMappingException, IOException {
		String url=getConfig(vastURL);
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> map = mapper.readValue(url, Map.class);
		mapper.writeValue(new File(GlobalConstants.LOCAL_ADTAG_MAP_JSON_FILENAME), map);
		return map;
		
	}
		
	/**
	 * Description : This method used for decode Ad Tag URL 
	 * @param url
	 * @return
	 */

	static String decodeURL(String url) {
		try {
			return java.net.URLDecoder.decode(url, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			//log.info("Exception occured while decoding url:"+e.getMessage());
			e.printStackTrace();
		}
		return url;
	}
	
	/**
	 * Description: Return encoded config
	 * @param config
	 * @return
	 */
	
	public static String encodeBase64(String configData){
		String configEncoded=null;
	try
	{
		
    byte[] encodedBytes = Base64.encodeBase64(configData.getBytes());
	configEncoded= new String(encodedBytes);
	} 
	catch (Exception e)
	{
		//log.info("Exception occured while decoding url:"+e.getMessage());
		e.printStackTrace();
	}
	
	return configEncoded;
}
	
	
	/**
	 *  Description : This method used for decode config of Ad tag URL
	 * @param object
	 * @return
	 */
	public JsonObject decodeBase64(String object){
		byte[] valueDecoded = Base64.decodeBase64(object);
		JsonParser jsonParser = new JsonParser();
		JsonObject jsonObject = jsonParser.parse(new String(valueDecoded)).getAsJsonObject();
		return jsonObject;
	}
	
	
	/**
	 * Description : Return JSON String of Ad tag config decode 
	 * 
	 * @param urlWithParams
	 * @param Ad_Type
	 * @return
	 * @throws MalformedURLException
	 * @throws UnsupportedEncodingException
	 */


	public static String getConfig(String vastURL)
			throws MalformedURLException, UnsupportedEncodingException {
		Map<String, String> query_pairs = new LinkedHashMap<String, String>();
		
		URL url=null;

		 url = new URL(vastURL);
		String[] pairs = url.getQuery().split("&");
		for (String pair : pairs) {
			int idx = pair.indexOf("=");
			query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"),
					URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
		}		
		String config = query_pairs.get(VastQueryParams.CONFIG.toString());
		String result1 = decodeURL(config);
		String result2 = decodeURL(result1);
		byte[] valueDecoded = Base64.decodeBase64(result2);
		JsonParser jsonParser = new JsonParser();
		JsonObject jsonObject = jsonParser.parse(new String(valueDecoded)).getAsJsonObject();
		return jsonObject.toString();
		
		 
	}
	
	/**
	 *  Description : Return ad params value from Ad tag URL 
	 * @param urlWithParams
	 * @param param
	 * @param Ad_Type
	 * @return
	 * @throws MalformedURLException
	 * @throws UnsupportedEncodingException
	 */
	
	public static String getparam(String vastURL, String param)
			throws MalformedURLException, UnsupportedEncodingException {
		Map<String, String> query_pairs = new LinkedHashMap<String, String>();
		
		URL url=null;
		
		 url = new URL(vastURL);

		String[] pairs = url.getQuery().split("&");
		for (String pair : pairs) {
			int idx = pair.indexOf("=");
			query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"),
					URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
		}		
		String adsParam = query_pairs.get(param);
		return adsParam;
		
	}
	
	
	/**
	 * 
	 * Description : Return asset list value from map
	 * @param url
	 * @param VastQueryparam
	 * @param ConfigAssetsParms
	 * @param Ad_Type
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	
	@SuppressWarnings("unchecked")
	public static Object listmapGenerator(String vastURL, String VastQueryparam, ConfigAssetsParms ConfigAssetsParms) throws JsonParseException, JsonMappingException, IOException
	
	{
	
		Object AssetList_param=null;
		List<LinkedHashMap<String, Object>> ASSET_LIST = (List<LinkedHashMap<String, Object>>) mapGenerator(vastURL).get(VastQueryparam);
		
		for(LinkedHashMap<String, Object> param: ASSET_LIST) {
			
	    AssetList_param = param.get(ConfigAssetsParms.toString());
	    System.out.println(AssetList_param);
	    
		}
		return  AssetList_param;
		
		
	}
	/**
	 * 
	 * @param vastURL
	 * @param VastQueryparam
	 * @param ConfigAssetsParms
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	
	@SuppressWarnings("unchecked")
public static Object listmapGenerator(String vastURL, String VastQueryparam, String ConfigAssetsParms) throws JsonParseException, JsonMappingException, IOException
	
	{
	
		Object AssetList_param=null;
		List<LinkedHashMap<String, Object>> ASSET_LIST = (List<LinkedHashMap<String, Object>>) mapGenerator(vastURL).get(VastQueryparam);
		
		for(LinkedHashMap<String, Object> param: ASSET_LIST) {
			
	    AssetList_param = param.get(ConfigAssetsParms.toString());
	    System.out.println(AssetList_param);
	    
		}
		return  AssetList_param;
		
		
	}
	/**
	 * 
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ParseException
	 */
	
	public static String readJson() throws FileNotFoundException, IOException, ParseException
	
	{
		
		JSONParser parser = new JSONParser();
    JSONObject jsonobject = (JSONObject) parser.parse(
            new FileReader(GlobalConstants.LOCAL_ADTAG_MAP_JSON_FILENAME));
    
      String adTagConfigDecodeJSON = jsonobject.toJSONString();
		return adTagConfigDecodeJSON;
		
	}
	
	/**
	 * Description: Update map into JSON file
	 * @param adTagMapData
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	
	public static void jsonUpdateMap(Map<String, Object> adTagMapData) throws JsonGenerationException, JsonMappingException, IOException
	{
		ObjectMapper mapper = new ObjectMapper();
		mapper.writeValue(new File(GlobalConstants.LOCAL_ADTAG_MAP_JSON_FILENAME), adTagMapData);
	}
	
	/**
	 * Description: Return Adparam value into map 
	 * @param vastURL
	 * @return
	 * @throws MalformedURLException 
	 * @throws UnsupportedEncodingException 
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	
	
public static Map<String, Object> mapAdparams(String vastURL) throws MalformedURLException, UnsupportedEncodingException  {
		
		Map<String, Object> map = new LinkedHashMap<String, Object>();
		URL url=null;
		 url = new URL(vastURL);
		String[] pairs = url.getQuery().split("&");
		for (String pair : pairs) {
			int idx = pair.indexOf("=");
			map.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"),
					URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
		}
		
		return map;
		
	}
	/**
	 * Descption: Return Updated Ad tag 
	 * @param adTagUrl
	 * @param adTagMapData
	 * @return
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws ParseException
	 */
	
	
	public static String  updatedConfigAdTag(String adTagUrl,  Map<String, Object> adTagMapData) throws JsonGenerationException, JsonMappingException, IOException, ParseException 
	{
		String UpdatedAdTag=null;
		
		AdTagMapper.jsonUpdateMap(adTagMapData);
		String configDecodeJson=AdTagMapper.readJson();
		String adTagConfig=AdTagMapper.encodeBase64(configDecodeJson);
		
		int indx=adTagUrl.indexOf("CONFIG")+7;
		String UpdatedFinalString=adTagUrl.substring(0, indx);
	    UpdatedAdTag=UpdatedFinalString + adTagConfig;
		return UpdatedAdTag;
	}
	
	/**
	 * 
	 * @param adTagUrl
	 * @return
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws ParseException
	 */
	
	public static String  testEnabledAdTag(String adTagUrl) throws JsonGenerationException, JsonMappingException, IOException, ParseException
	{
		String UpdatedAdTag=null;
			
		int indx=adTagUrl.indexOf("CONFIG");
		String AdTag=adTagUrl.substring(0, indx);
		String UpdatedTag=adTagUrl.substring(indx);
		String finalAdTag=AdTag +"TEST_ENABLED=true&"+UpdatedTag;
		UpdatedAdTag=finalAdTag;
		return UpdatedAdTag;
	}
	
}
	


	


