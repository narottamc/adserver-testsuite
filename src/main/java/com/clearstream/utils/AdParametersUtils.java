package com.clearstream.utils;

import static org.testng.Assert.assertEquals;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.adserver.AdParameters.models.ClearStream;
import com.adserver.AdParameters.models.Decisioning;
import com.adserver.AdParameters.models.Geo;
import com.adserver.AdParameters.models.RootAdParameters;
import com.adserver.mappers.VAST;
import com.adserver.AdParameters.models.Ad;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class AdParametersUtils {

	private static final Logger log = Logger.getLogger(AdParametersUtils.class);

	private static Gson gson = new GsonBuilder()
			.setFieldNamingStrategy(new com.adserver.AdParameters.models.FieldNamingStrategy()).create();

	public static void verifyDecisioningAdParameter(VAST objVAST, String paramName, boolean paramValue) {

		String AdParamJson = objVAST.getAd().get(0).getInLine().getCreatives().getCreative().get(0).getLinear()
				.getAdParameters();
		log.info("Ad Parameters Json :" + AdParamJson);
		RootAdParameters rootAdParams = gson.fromJson(AdParamJson, RootAdParameters.class);
		Decisioning decisioningParam = rootAdParams.getDecisioning();

		try {

			PropertyDescriptor objPropertyDescriptor = new PropertyDescriptor(paramName, Decisioning.class);
			Object variableValue = objPropertyDescriptor.getReadMethod().invoke(decisioningParam);
			assertEquals(variableValue, paramValue, "Actual Value of "+paramName+" Parameter is : " + " " + variableValue
					+ "And Expected Value is : " + paramValue);
			log.info("Actual Value of " + paramName + " parameter is : " + " " + variableValue
					+ " " + "And Expected Value is : " + paramValue);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| IntrospectionException e) {

			e.printStackTrace();
		}
	}
	
	public static void verifyDecisioningAdParameter(VAST objVAST, String paramName, String paramValue) {

		String AdParamJson = objVAST.getAd().get(0).getInLine().getCreatives().getCreative().get(0).getLinear()
				.getAdParameters();
		log.info("Ad Parameters Json :" + AdParamJson);
		RootAdParameters rootAdParams = gson.fromJson(AdParamJson, RootAdParameters.class);
		Decisioning decisioningParam = rootAdParams.getDecisioning();
		
		try {
			PropertyDescriptor objPropertyDescriptor = new PropertyDescriptor(paramName, Decisioning.class);
			Object variableValue = objPropertyDescriptor.getReadMethod().invoke(decisioningParam);
			assertEquals(variableValue, paramValue, "Actual Value of "+paramName+" Parameter is : " + " " + variableValue
					+ "And Expected Value is : " + paramValue);
			log.info("Actual Value of " + paramName + " parameter is : " + " " + variableValue
					+ " " + "And Expected Value is : " + paramValue);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| IntrospectionException e) {

			e.printStackTrace();
		}
		 }
	
	
	
	public static void verifyClearstreamParameterAdParameter(VAST objVAST, String paramName, double paramValue) {

		String AdParamJson = objVAST.getAd().get(0).getInLine().getCreatives().getCreative().get(0).getLinear()
				.getAdParameters();
		log.info("Ad Parameters Json :" + AdParamJson);
		RootAdParameters rootAdParams = gson.fromJson(AdParamJson, RootAdParameters.class);

		ClearStream clearStream = rootAdParams.getReporting().getClearstream();

		try {

			PropertyDescriptor objPropertyDescriptor = new PropertyDescriptor(paramName, ClearStream.class);
			Object variableValue = objPropertyDescriptor.getReadMethod().invoke(clearStream);
			assertEquals(variableValue, paramValue, "Actual Value of "+paramName+" Parameter is : " + " " + variableValue
					+ "And Expected Value is : " + paramValue);
			log.info("Actual Value of " + paramName + " parameter is : " + " " + variableValue
					+ " " + "And Expected Value is : " + paramValue);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| IntrospectionException e) {

			e.printStackTrace();
		}
	}
	
	public static void verifyClearstreamParameterAdParameter(VAST objVAST, String paramName, int paramValue) {

		String AdParamJson = objVAST.getAd().get(0).getInLine().getCreatives().getCreative().get(0).getLinear()
				.getAdParameters();
		log.info("Ad Parameters Json :" + AdParamJson);
		RootAdParameters rootAdParams = gson.fromJson(AdParamJson, RootAdParameters.class);

		ClearStream clearStream = rootAdParams.getReporting().getClearstream();

		try {

			PropertyDescriptor objPropertyDescriptor = new PropertyDescriptor(paramName, ClearStream.class);
			Object variableValue = objPropertyDescriptor.getReadMethod().invoke(clearStream);
			assertEquals(variableValue, paramValue, "Actual Value of "+paramName+" Parameter is : " + " " + variableValue
					+ "And Expected Value is : " + paramValue);
			log.info("Actual Value of " + paramName + " parameter is : " + " " + variableValue
					+ " " + "And Expected Value is : " + paramValue);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| IntrospectionException e) {

			e.printStackTrace();
		}
	}
	
	
	public static void verifyAdTagTypeParameter(VAST objVAST, String paramName, String paramValue)   {

		String AdParamJson = objVAST.getAd().get(0).getInLine().getCreatives().getCreative().get(0).getLinear()
				.getAdParameters();
		log.info("Ad Parameters Json :" + AdParamJson);
		RootAdParameters rootAdParams = gson.fromJson(AdParamJson, RootAdParameters.class);
		
		try {

			PropertyDescriptor objPropertyDescriptor = new PropertyDescriptor(paramName, RootAdParameters.class);
			Object variableValue = objPropertyDescriptor.getReadMethod().invoke(rootAdParams);
			
			
			assertEquals(variableValue, paramValue, "Actual Value of "+paramName+" Parameter is : " + " " + variableValue
					+ "And Expected Value is : " + paramValue);
			log.info("Actual Value of " + paramName + " parameter is : " + " " + variableValue
					+ " " + "And Expected Value is : " + paramValue);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| IntrospectionException e) {

			e.printStackTrace();
		}
	}
	
	
	public static void verifyAdParameter(VAST objVAST, String paramName, String paramValue) {

		String AdParamJson = objVAST.getAd().get(0).getInLine().getCreatives().getCreative().get(0).getLinear()
				.getAdParameters();
		log.info("Ad Parameters Json :" + AdParamJson);
		RootAdParameters rootAdParams = gson.fromJson(AdParamJson, RootAdParameters.class);
		Ad ad = rootAdParams.getAd();

		try {

			PropertyDescriptor objPropertyDescriptor = new PropertyDescriptor(paramName, Ad.class);
			Object variableValue = objPropertyDescriptor.getReadMethod().invoke(ad);
			assertEquals(variableValue, paramValue, "Actual Value of "+paramName+" Parameter is : " + " " + variableValue
					+ "And Expected Value is : " + paramValue);
			log.info("Actual Value of " + paramName + " parameter is : " + " " + variableValue
					+ " " + "And Expected Value is : " + paramValue);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| IntrospectionException e) {

			e.printStackTrace();
		}
	}
	
	public static void verifyGeoAdParameter(VAST objVAST, String paramName, boolean paramValue) throws NoSuchFieldException, SecurityException {

		String AdParamJson = objVAST.getAd().get(0).getInLine().getCreatives().getCreative().get(0).getLinear()
				.getAdParameters();
		log.info("Ad Parameters Json :" + AdParamJson);
		RootAdParameters rootAdParams = gson.fromJson(AdParamJson, RootAdParameters.class);

		try {

			PropertyDescriptor objPropertyDescriptor = new PropertyDescriptor(paramName, RootAdParameters.class);
			Object variableValue = objPropertyDescriptor.getReadMethod().invoke(rootAdParams);
		 Class<?> geo = variableValue.getClass();
			  Field field = geo.getDeclaredField("enabled");
			    Object fieldValue = field.get(variableValue);
		
			assertEquals(fieldValue, paramValue, "Actual Value of "+paramName+" Parameter is : " + " " + fieldValue
					+ "And Expected Value is : " + paramValue);
			log.info("Actual Value of " + paramName + " parameter is : " + " " + variableValue
					+ " " + "And Expected Value is : " + paramValue);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| IntrospectionException e) {

			e.printStackTrace();
		}
	}
	
	public static void verifyTestAdParameter(VAST objVAST, String paramName, boolean paramValue) throws NoSuchFieldException, SecurityException {

		String AdParamJson = objVAST.getAd().get(0).getInLine().getCreatives().getCreative().get(0).getLinear()
				.getAdParameters();
		log.info("Ad Parameters Json :" + AdParamJson);
		RootAdParameters rootAdParams = gson.fromJson(AdParamJson, RootAdParameters.class);

		try {

			PropertyDescriptor objPropertyDescriptor = new PropertyDescriptor(paramName, RootAdParameters.class);
			Object variableValue = objPropertyDescriptor.getReadMethod().invoke(rootAdParams);
		 Class<?> test = variableValue.getClass();
			  Field field = test.getDeclaredField("enabled");
			    Object fieldValue = field.get(variableValue);
		
			assertEquals(fieldValue, paramValue, "Actual Value of "+paramName+" Parameter is : " + " " + fieldValue
					+ "And Expected Value is : " + paramValue);
			log.info("Actual Value of " + paramName + " parameter is : " + " " + variableValue
					+ " " + "And Expected Value is : " + paramValue);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| IntrospectionException e) {

			e.printStackTrace();
		}
	}

}
