package com.clearstream.utils;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.io.IOUtils;

import com.adserver.mappers.VAST;

public class AdTagFactory {
	
	private VAST vast ;
    private AdTagFactory(String respXMLString) throws JAXBException, IOException {
    	JAXBContext jaxbContextVAST = JAXBContext.newInstance(com.adserver.mappers.VAST.class);
		Unmarshaller unmarshallerVAST = jaxbContextVAST.createUnmarshaller();
		InputStream respInputStream = IOUtils.toInputStream(respXMLString, "UTF-8");
		vast = (com.adserver.mappers.VAST) unmarshallerVAST.unmarshal(respInputStream);
    }
     
    private static class Holder {
        private static String XmlString;
        private static AdTagFactory INSTANCE ; 
        private static AdTagFactory getInstance() throws JAXBException, IOException{
        	 INSTANCE = new AdTagFactory(XmlString); 
        	return INSTANCE;
        }
    }
     
    public static AdTagFactory getInstance(String responseXMLString) throws JAXBException, IOException {
    	Holder.XmlString = responseXMLString;
        return Holder.getInstance();
    }

	public VAST getVast() {
		return vast;
	}

	public void setVast(VAST vast) {
		this.vast = vast;
	}
     
    
}
