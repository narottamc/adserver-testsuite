package com.clearstream.utils;

import com.adserver.models.AdTagsConfig.AdtagsTypes;

public class QueryBuilder {
	

	/**
	 * Description: Return query for VAST/VPAID for Video ads tag.
	 * @param Asset_Type
	 * @param Ad_Type
	 * @return
	 */
	public String getQuery(AdtagsTypes Asset_Type, AdtagsTypes Ad_Type)
	{
		  String query=null;
   
			 if(Asset_Type.equals(AdtagsTypes.VPAID_TYPE))
			 {
			 query = "select CMP.id as campaign_id, CMP.name as campaign_name, PLC.id as placement_id, "
					+ " PLC.name as placement_name, FLTS.id as flight_id, FLTS.name as flight_name, "
					+ " AD.id as ad_id, AD.name as ad_name, AD.ad_type as ad_type, AD.ad_tag as ad_tag, "
					+ " AGN.id as agency_id, AGN.name as agency_name, BRND.id as brand_id, "
					+ " BRND.name as brand_name from ads AD join flights FLTS ON AD.flight_id = FLTS.id "
					+ " join placements PLC ON FLTS.placement_id = PLC.id "
					+ " join placement_groups PG ON PLC.placement_group_id = PG.id "
					+ " join campaigns CMP ON PG.campaign_id = CMP.id join agencies AGN ON CMP.agency_id = AGN.id "
					+ " 			join brands BRND ON CMP.brand_id = BRND.id  "

					+ " where CMP.state_id = 2 and AD.ad_type = '"+Ad_Type+"' and FLTS.end_date > now() "
					+ " and FLTS.placement_id is not null  and CMP.brand_id is not null "
					+ " and AD.budget_percentage > 0  " + " and PLC.id  in ( "
					+ " select distinct placement_id from assets_placements ASP "
					+ " join assets ASTS ON ASP.asset_id = ASTS.id " + " 	where ASTS.is_v_paid = true " + " ) limit 1";
			 }
			 if(Asset_Type.equals(AdtagsTypes.VAST_TYPE))
			 {
				 
				  query = "select CMP.id as campaign_id, CMP.name as campaign_name, PLC.id as placement_id, "
							+ " PLC.name as placement_name, FLTS.id as flight_id, FLTS.name as flight_name, "
							+ " AD.id as ad_id, AD.name as ad_name, AD.ad_type as ad_type, AD.ad_tag as ad_tag, "
							+ " AGN.id as agency_id, AGN.name as agency_name, BRND.id as brand_id, "
							+ " BRND.name as brand_name from ads AD join flights FLTS ON AD.flight_id = FLTS.id "
							+ " join placements PLC ON FLTS.placement_id = PLC.id "
							+ " join placement_groups PG ON PLC.placement_group_id = PG.id "
							+ " join campaigns CMP ON PG.campaign_id = CMP.id join agencies AGN ON CMP.agency_id = AGN.id "
							+ " 			join brands BRND ON CMP.brand_id = BRND.id  "

							+ " where CMP.state_id = 2 and AD.ad_type = '"+Ad_Type+"' and FLTS.end_date > now() "
							+ " and FLTS.placement_id is not null  and CMP.brand_id is not null "
							+ " and AD.budget_percentage > 0  " + " and PLC.id  in ( "
							+ " select distinct placement_id from assets_placements ASP "
							+ " join assets ASTS ON ASP.asset_id = ASTS.id " + " 	where ASTS.is_v_paid = false " + ") limit 1";
				 
			 }
			 
			
			 
			return query;
			
}
	
	/**
	 * Description: Return query for survey ad tags.
	 * @param content_type
	 * @return
	 */
	
	public String getQuery_Survey(AdtagsTypes content_type)
	{
		  String query=null;
   
			 
			 if(content_type.equals(AdtagsTypes.SURVEY_CONTENT))
			 {
				 
				  query = "select CMP.id as campaign_id, CMP.name as campaign_name, PLC.id as placement_id, "
							+ " PLC.name as placement_name, FLTS.id as flight_id, FLTS.name as flight_name, "
							+ " AD.id as ad_id, AD.name as ad_name, AD.ad_type as ad_type, AD.ad_tag as ad_tag, "
							+ " AGN.id as agency_id, AGN.name as agency_name, BRND.id as brand_id, "
							+ " BRND.name as brand_name from ads AD join flights FLTS ON AD.flight_id = FLTS.id "
							+ " join placements PLC ON FLTS.placement_id = PLC.id "
							+ " join placement_groups PG ON PLC.placement_group_id = PG.id "
							+ " join campaigns CMP ON PG.campaign_id = CMP.id join agencies AGN ON CMP.agency_id = AGN.id "
							+ " 			join brands BRND ON CMP.brand_id = BRND.id  "

							+ " where CMP.state_id = 2 and AD.ad_type = 'HTML' and FLTS.end_date > now() "
							+ " and FLTS.placement_id is not null  and CMP.brand_id is not null "
							+ " and AD.budget_percentage > 0  " + " and PLC.id  in ( "
							+ " select distinct placement_id from assets_placements ASP "
							+ " join assets ASTS ON ASP.asset_id = ASTS.id " + " and content_type='"+content_type+"' ) limit 1";
				 
			 }
			  
			return query;
}
	
	}



