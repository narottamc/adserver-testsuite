package com.clearstream.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.ini4j.InvalidFileFormatException;

import com.adserver.models.AdTagsConfig.AdtagsTypes;

public class AdTagGenerator {

	private String query;
	//private Connection conn_;
	private PreparedStatement statement_; 
	private ResultSet result_;
	private static CreatePostGresConnection connection = new CreatePostGresConnection();
	
	
	
	
	/**
	 * Description: Return Video Ad tag from Postgres database
	 * @param Asset_Type
	 * @param Ad_Type
	 * @return
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws InvalidFileFormatException 
	 */

	public String getVideoAlchemyAdtag(AdtagsTypes Asset_Type, AdtagsTypes Ad_Type, Connection conn_) throws InvalidFileFormatException, FileNotFoundException, IOException {

//		CreatePostGresConnection connection = new CreatePostGresConnection();
//		connection.setConnection();
//		conn_ = connection.getConnection();
		QueryBuilder bulder = new QueryBuilder();
		query = bulder.getQuery(Asset_Type, Ad_Type);
		String ad_tag = null;
		
		try {
			statement_ = conn_.prepareStatement(query);
			result_ = statement_.executeQuery();

				while (result_.next()) {
					if(Ad_Type.equals(AdtagsTypes.HTML_AD))
					{
						ad_tag = result_.getString("ad_tag");
						int idx1=ad_tag.indexOf("src='")+5;
				        int idx2=ad_tag.indexOf("style")-2;
				        ad_tag=ad_tag.substring(idx1, idx2);
						
					}
					else 
					{
					ad_tag = result_.getString("ad_tag");
					}
				
				}
			}
		 catch (SQLException e) {
			System.out.println(e.getMessage());
		}

		connection.closeConnection(conn_);

		return ad_tag;
	}
	
	/**
	 *  Description: Return Survey Ad tag from Postgres database
	 * @param content_type
	 * @return
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws InvalidFileFormatException 
	 */

	public String getSurveyAdtag(AdtagsTypes content_type,Connection conn_) throws InvalidFileFormatException, FileNotFoundException, IOException {

//		CreatePostGresConnection connection = new CreatePostGresConnection();
//		connection.setConnection();
//		conn_ = connection.getConnection();
		QueryBuilder bulder = new QueryBuilder();
		query = bulder.getQuery_Survey(content_type);
		String ad_tag = null;
		
		try {
			statement_ = conn_.prepareStatement(query);
			result_ = statement_.executeQuery();

			while (result_.next()) {
				
				if(content_type.equals(AdtagsTypes.SURVEY_CONTENT))
				{
					ad_tag = result_.getString("ad_tag");
					int idx1=ad_tag.indexOf("src='")+5;
			        int idx2=ad_tag.indexOf("style")-2;
			        ad_tag=ad_tag.substring(idx1, idx2);
					
				}
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

		connection.closeConnection(conn_);

		return ad_tag;
	}
	
}
